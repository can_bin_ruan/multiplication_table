﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //九九乘法表
            for(int i = 1; i <= 9; i++)
            {
                Console.WriteLine();
                for(int j=1; j<=i; j++)
                {
                    Console.Write(j + "*" + i + "="+ j * i+"\t");
                }
            }
            //菱形
            Console.WriteLine();
            Console.WriteLine("请输菱形的高度的一半");
            int t = int.Parse(Console.ReadLine());
            for(int j = 1; j <= t; j++)
            {
                for(int a = 1; a <= t-j; a++)
                {
                    Console.Write(" ");
                }
                for(int b = 1; b <= 2 * j - 1; b++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
            for(int d = t-1; d >=1; d--)
            {
                
                for(int e = 1; e<=t-d; e++)
                {
                    Console.Write(" ");
                }
                for(int f = 1; f <= 2 * d - 1; f++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }
    }
}
