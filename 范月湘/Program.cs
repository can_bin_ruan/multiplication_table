﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("九九乘法表");
            for (int i=1;i<=9;i++)
            {
                for (int j=1;j<=i;j++)
                {
                    Console.Write(j+"*"+i+"="+i*j+" ");
                }
                Console.WriteLine();
            }
        }
    }
}
